# GUT: General Unified Template

> GUT is the General Unified Template, the binary format that will define the shape of any General Unified Data.

## Is GUT a coincidence?

This section intentionally left blank.

## Anyway

So before defining any kind of format for some specific binary data structures, if we aim at having a *general* protocol, the best thing to do would be to define a very simple template for any kind of data structure.

Well, here's the simplest way that I can think of: having just one byte for the data type, and the remaining bytes for the actual data structure.
This byte will be used to choose the parser to use for the next bytes.

### Only one byte?

Now the main drawback about this is that I would only allow a total number of 256 types of data structures.
Will it be enough? Well, I don't know, I feel like it is.
I don't remember seeing that many from my data structures class.

### One more thing

I don't think that just having one byte for the data type is gonna make it.
By that, I don't mean that there aren't enough bytes for the data type.
I mean that it's not enough information to include in our template.
I want to include one more thing: *the size in bytes of the data structure*.
I think it is an important thing to include, as such a small amount of information allows for partial / lazy parsing of the information, since accessing a byte at a given index is of complexity *O(1)*.

However, it may not be necessary for every data structure.
In fact, if I'm mentioning partial and lazy parsing as a benefit, it must mean that I'm thinking about *dynamically sized* data types, such as an array.

Thus, here's the best thing to do: if the data type is statically sized, which means that a data structure of that type will always have the same amount of bytes, then the template will just be the first single byte representing the data type.
But if the data type is dynamically sized, which means that a data structure of that type will not always have the same amount of bytes, then the template will be the first single byte representing the data type, **followed directly** by an additional number that will represent the size in bytes of the entire structure.

### `u32` or `u64`?

> Note: `u` in `u32` and `u64` stands for **u**nsigned integer. It comes from Rust primitive data types.

- With a `u32` integer, the maximum length of a data structure is **4 294 967 296 B**, or exactly **4 GiB**.
- With a `u64` integer, it becomes **18 446 744 073 709 551 616 B**, or **17 179 869 184 GiB**, or exactly **16 EiB** (1 exbibyte = 1024^6 bytes).

Now while it's true that my main focus is for programs to communicate in a better way, I think it is important to consider the maximum value when it comes to files.
I don't know how important the possibility for a data structure to be heavier than 4 GiB in size can be, but if it can't, then in the future, we'll have to find hacks or redefine some portion of the protocol for both of them to work. I will go for `u64`, a full on 8 bytes taken to describe the size of the structure.

Also, obviously, integer values are gonna be written in Little Endian instead of Big Endian, since it's just easier for computers to parse.

## In the end

The GUD protocol defines a GUT (General Unified Template) as follows:

- statically-sized data structure
  One byte for the data type, and the remaining bytes for the actual data.
  ```rust
  struct SomeStaticData {
    data_type: u8,
    // ...
  }
  ```

- dynamically-sized data structure
  One byte for the data type, then 8 bytes for the size of the data structure starting from the remaining bytes.
  ```rust
  struct SomeDynamicData {
    data_type: u8,
    size: u64,
    // ...
  }
  ```
