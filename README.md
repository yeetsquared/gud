# GUD: General Unified Data

> GUD is (will be (or will it?)) a protocol to communicate using **binary data structures** instead of raw text.

## Is GUD a coincidence?

This section intentionally left blank.

## Synopsis

Don't think too harshly about this protocol. It's probably gonna stay as an idea on top of my head.
Worse, it has probably already been done, and people have probably already built an entire operating system around this idea. (thinking about Plan9 and the 9p protocol).
If this thought process of mine doesn't go anywhere due to it being pretty much material recycled from the void of my ignorance, then let it exist for educational or entertaining purposes.
Essentially, the idea is to provide a better way to make programs communicate to each other, using actual data instead of raw text.

I stumbled upon the DION account on Twitter some day. The first thing I was amazed at was the animations of their editor prototype. The second thing I was amazed at was the options available for different forms of code refactoring. The last thing I was amazed at was what they were actually working on: very similarly to what I'm doing right now, a file format for generalized abstract syntax trees. Because, for the exact same reasons I'm sketching GUD right now, they noticed that there are a lot of patterns in every programming language and that there's a low level part in all parsers - most specifically tokenizing - which would be better off as a common ground.

DION was actually the main inspiration for this idea. Just as they notice a common ground between all programming languages, I notice a common ground between all forms of data communication.

## Why not just use raw text?

In every program, whenever they have to communicate to the external world, they have to send a sequence of bytes that are gonna be interpreted according to some language or protocol. And each and every time, individual parsers have to be written for them.
Some syntaxes are meant to be simple and straight-forward, like Lisp or JSON. Some are meant to be more readable, like YAML. And some are meant to be optimal for specific programming languages, like RON for Rust.
But at the end of the day, all of them represent *data structures*. Thus, I believe there has to be a way to abstract over all of them.

## What would be the benefits?

It would be easier for programs to communicate to each other, and also easier for users to express commands, as they would talk not in raw text but in actual data structures, under a format common to *every single* program. Not only that, but it can be extended to code as well: we could write code using this General Unified Data format instead of each language having to implement its own parser from the ground up.

Let's suppose you decide to build an entire operating system around this data protocol. Then it would, in my view, be much easier for you to think of your programs as dealing with data structures instead of raw text. As a result, if you make everything from the ground up, every application, including the shell, terminal, various text editing, programming, sketching, is gonna be impacted by that way of communicating.

Think about it: the terminal, as of today, is literally just a text displayer. It did gain more features over the years, such that there is now a *terminal capability data base* called `terminfo` that specifies basic things a terminal should be able to do. But it is *nowhere near* as abstract as it could be. While it can occasionally send meta commands through the escape character, it still fundamentally displays text.

Shells too: there is an entire text parser for shell commands, whether it is in Bash, Dash, Zsh, Fish, ... Or even Batch or Powershell. To communicate with your shell, you send a readable sequence of characters, and leave the job to the shell parser, specifically written for its shell, to correctly parse the command you sent it: separate arguments, evaluate globs, detect pipes, and especially detect and report syntax errors in a user-friendly way.

At the end of the day, what happens is that developers tend to focus on coding the parser itself. And when that is done, they likely won't bother to implement additionnal features such as syntax highlighting, live error detection, and autocompletion. If we were to make our programs communicate directly through a General Unified Data format, everything would be less overhead: no need to think about syntax ambiguity, syntax design choices, the performance and complexity of the parser... We would just use those data structures everywhere, and in return, it would become much, much easier to implement more high-level features such as syntax highlighting, live error detection, and autocompletion.

## What now?

Now you can read my other thoughts that I put down in other markdown files out there!
